FROM node

RUN mkdir /skillbox

WORKDIR /skillbox
COPY package.json /skillbox
RUN yarn install


copy . /skillbox


## Setup and running


RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
